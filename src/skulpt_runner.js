function sanitizeHTML(text) {
    var element = document.createElement('div');
    element.innerText = text;
    return element.innerHTML;
}

class CodingBox {
    constructor(type, element, index, path) {
        this.settings = "none";
        this.id = "none";
        this.codingbox = "none";
        this.path = path
        this.editor_id = "none";
        this.editor = "none";
        this.canvas_id = "none";
        this.console_id = "none";
        this.stop_execution = false;
        this.Sk = "none";
        this.running = false;

        if (type == "codefield") {
            this.from_codefield(element, index)
        }
    }

    get_code() {
        return this.codingbox.querySelector(".code").textContent;
    }

    set_code(code) {
        this.editor.getSession().setValue(code);
    }

    from_codefield(code_field, index) {
        code_field.classList.add("skulpt_runner_prepared");
        /* Wrap code_field in skulpt_codefield div */
        /* Get JSON prefix */
        var lines = code_field.textContent.split('}')
        var json_firstline = lines[0].trim() + "}";
        lines.splice(0, 1);
        var code_string = lines.join('\n').replace(/.*/, "").substr(1);
        code_field.style.display = "none";
        this.settings = generate_settings(JSON.parse(json_firstline), code_field.className)
        this.codingbox = document.createElement('div');
        this.codingbox.classList.add("codingbox");
        if (code_field.closest("pre") != null) {
            var pre = code_field.closest("pre")
            pre.parentElement.insertBefore(this.codingbox, pre);
            code_field.closest("pre").style.display = "none";
        } else {
            code_field.parentElement.insertBefore(this.codingbox, code_field);
        }
        this.set_up_codingbox_with_template(code_string, index);
    }

    set_up_codingbox_with_template(code, index) {
        /* Basis code for a codingbox
         */
        this.id = "codingbox_" + index
        this.codingbox.id = this.id
        this.src_tab = "src_tab" + index
        this.canvas_tab = "canvas_tab" + index
        this.codingbox.innerHTML =
            `
        <div class="code_row" style="display:none">
            <textarea class="code" cols="40" style="display:none;"></textarea>
        </div>
        <!--
        <div class="spinner lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        </div>
        -->
        <div class="tabbed">
        <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="${this.src_tab}_toggle" data-toggle="tab" href="#${this.src_tab}" role="tab" aria-controls="home" aria-selected="true">Source</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="${this.canvas_tab}_toggle" data-toggle="tab" href="#${this.canvas_tab}" role="tab" aria-controls="profile" aria-selected="false">Canvas</a>
        </li>
      </ul>
      <div class="tab-content">
      <div class="tab-pane src-tab fade show active" id="${this.src_tab}" role="tabpanel" aria-labelledby="Src-Tab">
         <div class="editor_row">
            <div class="editor" cols="40" rows="10"></div>
        </div>  
      </div>
      <div class="tab-pane canvas-tab fade" id="${this.canvas_tab}" role="tabpanel" aria-labelledby="Canvas Tab">
            <div class="canvas-wrapper">
            <div class="canvas" height="400px" style="display:none;">
            </div>
            </div>
      </div>
      </div>
      </div>
      <div class="console_row"></div>
        <div class="buttons_row"></div>
        `

        this.codingbox.querySelector(".code").textContent = code;
    }

    mark_processed() {
        codingbox = this.codingbox;
        if (codingbox.classList.contains("skulpt_runner_processed")) {
            codingbox.classList.add("skulpt_runner_processed")
        }
        var body = document.querySelector("body")
        if (!body.classList.contains("skulpt_runner")) {
            body.classList.add("skulpt_runner")
        }
    }

    add_listeners() {
        if (this.settings.options.includes("buttons")) {
            var run_button = this.codingbox.querySelector(".buttons_row .run")
            run_button.addEventListener("click", () => {
                this.run()
            });
            var clear_button = this.codingbox.querySelector(".buttons_row .clear")
            clear_button.addEventListener("click", () => {
                console.info("start clear");
                this.clear()
            });
        }

        if (this.settings.options.includes("autostart")) {
            // this.run();
            if (document.querySelector("#" + this.editor_id) != null) {
                document.querySelector("#" + this.editor_id).addEventListener("change", function(event) {
                    this.run()
                });
            }
        }
        if (this.codingbox.querySelector(".save-button") != null) {
            this.codingbox.querySelector(".save-button").addEventListener("click", function() {

                var blob = new Blob([codingbox_obj.get_code()], {
                    type: "text/plain;charset=utf-8"
                });
                saveAs(blob, "my_code.py");
            });
        }
        if (this.codingbox.querySelector(".load-button") != null) {
            this.codingbox.querySelector(".load-button").addEventListener("click", function() {
                this.codingbox.querySelector(".load-browse").click();
            });
        }
        if (this.codingbox.querySelector(".load-browse") != null) {
            this.codingbox.querySelector(".load-browse").addEventListener("change", function() {
                var selectedFile = this.files[0];
                const reader = new FileReader();
                reader.readAsText(selectedFile);
                reader.onload = function() {
                    codingbox_obj.set_code(reader.result);
                };
            });
        }
    }



    add_buttons_to_buttons_row() {
        var codingbox_obj = this;
        if (this.settings.options.includes("buttons")) {
            var row = this.codingbox.querySelector(".buttons_row")

            var buttons = '<a class="btn run" role="button"><i class="fa fa-play"></i>Run</a>' +
                '<a class="btn clear" role="button"><i class="fa fa-stop"></i>Stop\/Clear</a>'
            if (codingbox_obj.settings.options.includes("load-save")) {
                buttons += '<a class="btn save-button" >Save</a>' +
                    '<a href="#" class="btn load-button">Load</a> <input type="file" class="load-browse" accept=".py" style="display: none;">'
            }
            row.innerHTML = buttons


        } else {
            this.codingbox.querySelector(".buttons_row").style.display = "none";
        }
    }



    add_console_to_console_row() {
        this.console_id = codingbox.id + "_console";
        var row = codingbox.querySelector(".console_row");
        var console_wrapper = document.createElement('div');
        console_wrapper.classList.add("console-wrapper")
        row.append(console_wrapper)
        console_wrapper.innerHTML = '<h3>Konsole</h3>' + '<textarea disabled class="console" id="' + this.console_id + '"></textarea>'
        if (this.settings.options.includes("console")) {
            this.codingbox.querySelector(".console_row").style.display = "block";
        } else {
            this.codingbox.querySelector(".console_row").style.display = "none";
        }
    }

    add_editor_to_editor_row() {
        var codingbox_obj = this;
        if (this.settings.options.includes("editor")) {
            var row = codingbox.querySelector(".editor_row")
            window.require = requirejs;
            requirejs(["ace/ace"], function(ace) {
                codingbox_obj.editor_id = codingbox_obj.id + "_ace_editor";
                codingbox_obj.codingbox.querySelector(".editor").id = codingbox_obj.editor_id;
                codingbox_obj.editor = ace.edit(codingbox_obj.editor_id);
                codingbox_obj.editor.session.setValue(codingbox.querySelector(".code").textContent.trim().replace(/(<([^>]+)>)/ig, ""))
                codingbox_obj.editor.setFontSize("18px");
                codingbox_obj.editor.session.setMode("ace/mode/python");
                codingbox_obj.editor.getSession().on('change', function() {
                    var code_area = codingbox.querySelector(".code_area");
                    var code = codingbox.querySelector(".code");
                    if (code_area !== null) {
                        code_area.value = codingbox_obj.editor.getSession().getValue();
                    }
                    if (code !== null) {
                        code.textContent = codingbox_obj.editor.getSession().getValue();
                    }
                });
                if (codingbox_obj.settings !== "none") {
                    // First: Check if rows is set.
                    if (codingbox_obj.settings.hasOwnProperty("rows")) {
                        var rows = parseInt(sanitizeHTML(codingbox_obj.settings["rows"]))
                        var min_lines = rows;
                        var max_lines = rows
                    } else { // Else: Check if min_lines or max_lines is set
                        if (codingbox_obj.settings.hasOwnProperty("min_lines")) {
                            var min_lines = parseInt(sanitizeHTML(codingbox_obj.settings["min_lines"]))
                        } else {
                            var min_lines = 10
                        }
                        if (codingbox_obj.settings.hasOwnProperty("max_lines")) {
                            var max_lines = parseInt(sanitizeHTML(codingbox_obj.settings["max_lines"]))
                        } else {
                            var max_lines = 10
                        }
                    }
                    // if nothing is set
                } else {
                    var min_lines = 10;
                    var max_lines = 10
                }
                // if fullscreen
                if (codingbox_obj.settings.hasOwnProperty("display")) {
                    if (codingbox_obj.settings.display.includes("fullscreen")) {
                        var min_lines = 10;
                        var max_lines = Infinity;

                        codingbox_obj.editor.on("change", function() {
                            var lineHeight = codingbox_obj.editor.renderer.lineHeight;
                            document.querySelector("#" + codingbox_obj.editor_id).style.height = lineHeight + "px";
                            codingbox_obj.editor.resize();
                        });

                    }
                }
                codingbox_obj.editor.setTheme("ace/theme/monokai");
                codingbox_obj.editor.setOptions({
                    minLines: min_lines,
                    maxLines: max_lines,
                    tabSize: 4,
                    useSoftTabs: true
                });
            });
        } else {
            this.codingbox.querySelector(".console_row").style.display = "none";
        }
    }


    add_canvas_to_canvas_row() {
        this.canvas_id = this.codingbox.id + "_canvas";
        if (this.settings.options.includes("canvas")) {
            var canvas = this.codingbox.querySelector(".canvas")
            canvas.id = this.canvas_id;
            canvas.style.display = "block";
            if (this.settings.options.includes("turtle")) {
                canvas.parentElement.classList.add("turtle")
            }
        } else {
            this.codingbox.querySelector("#" + this.canvas_tab + "_toggle").style.display = "none";
        }
    }

    modify_display() {
        if (this.settings.display && this.settings.display.includes("sketch")) {
            this.codingbox.classList.add("display_sketch")
        }
        if (this.settings.display && this.settings.display.includes("fullscreen")) {
            this.codingbox.classList.add("display_fullscreen")
        }
    }

    default_settings() {
        // add canvas for missing libraries
        if (this.settings.options != null && this.settings.options.includes("processing")) {
            if (!this.settings.options.includes("canvas")) {
                this.settings += "canvas";
            }
        }
        if (this.settings.preset != null) {
            if (this.settings.preset.includes("calculator")) {
                this.settings.options = "editor buttons console"
            }
            if (this.settings.preset.includes("processing")) {
                this.settings.options = "editor buttons console canvas processing"
            }
            if (this.settings.preset.includes("turtle")) {
                this.settings.options = "editor buttons console canvas turtle"
            }
            if (this.settings.preset.includes("processing_sketch")) {
                this.settings.options = "buttons canvas processing"
                this.settings.display = "sketch"
            }
            if (this.settings.preset.includes("turtle_sketch")) {
                this.settings.options = "buttons canvas turtle"
                this.settings.display = "sketch"
            }
        }
    }

    process() {
        this.mark_processed();
        this.default_settings();
        this.add_buttons_to_buttons_row();
        this.add_console_to_console_row();
        this.add_editor_to_editor_row();
        this.add_canvas_to_canvas_row();
        this.modify_display();
        this.add_listeners();
        requirejs(["skulpt"], function(Sk) {
            requirejs(["skulpt-stdlib"]);
        });
        this.codingbox.className += " " + this.settings.options
    }

    handle_exception(error) {
        var Sk = this;
        var error_message = error.args.v[0].$mangled
        var lineno = error.traceback[0].lineno
        var colno = error.traceback[0].colno
        Sk.output("Error: " + error_message + " on line " + lineno)
    }
    // Print to console
    write_to_console(text) {
        var Sk = this;
        var lastline = "";
        var console_counter = 1;
        if (Sk.pre !== undefined) {
            var textarea = document.querySelector("#" + Sk.pre)
        } else {
            var textarea = document.querySelector("#" + this.console_id)
        }

        if (textarea != null) {
            var content = textarea.value;
            if (content != undefined) {
                var lines = content.split("\n");
                lastline = lines[lines.length - 2];
                if (lastline == undefined || lastline == "") {
                    console_counter = 1;
                } else if (lastline.startsWith("#")) {
                    console_counter = lastline.slice(lastline.indexOf("#") + 1, lastline.indexOf(":"))
                    console_counter++;
                };
            }
            var output = "#" + console_counter + ": " + text;
            if (!output.endsWith("\n")) {
                output += "\n"
            }
            textarea.value += output
            textarea.scrollTop = textarea.scrollHeight;
        }
    }


    builtinRead(x) {
        if (Sk.builtinFiles === undefined || Sk.builtinFiles["files"][x] === undefined)
            throw "File not found: '" + x + "'";
        return Sk.builtinFiles["files"][x];
    }

    prepare_processing(src) {
        if (!src.includes("setup()")) {
            src = "def setup():\n" + "    " + src.replace('\n', '\n    ')
        }
        src = "from p5 import *\n" + src + "\nrun()";
        return src;
    }

    clear() {
        this.stop_execution = true;
        if (document.querySelector("#" + this.canvas_id) != null) {
            document.querySelector("#" + this.canvas_id).innerHTML = "";
        }
        console.info("clear canvas")
        if (this.running) {
            this.stop()
        }

        this.running = false;

    }

    toggle_canvas_tab() {
        if (this.settings.options.includes("canvas")) {
            $(this.codingbox).find("a#" + this.canvas_tab + "_toggle").tab("show");
        }

    }

    stop() {
        if (this.running == true) {
            if (this.settings["options"].includes("processing")) {
                if (this.Sk !== undefined && this.Sk.p5 !== undefined) {
                    this.Sk.p5.instance.remove();
                }
            }
        }
    }



    run() {
        this.running = false;
        this.clear();
        var src = this.get_code();
        var codingbox_obj = this;
        requirejs(["skulpt", "p5", "jquery", "bootstrap"], function(Sk, p5, $, b) {
            requirejs(["skulpt-stdlib"]);
            codingbox_obj.toggle_canvas_tab(codingbox_obj);
            Sk = window.Sk
            codingbox_obj.Sk = Sk
            window.p5 = p5
            var skulpt_console = document.querySelector("#" + codingbox_obj.console_id);
            Sk.pre = codingbox_obj.console_id;
            Sk.p5Sketch = codingbox_obj.canvas_id;
            Sk.console_counter = 0;
            skulpt_console.value = '';
            Sk.configure({
                output: codingbox_obj.write_to_console,
                uncaughtException: codingbox_obj.handle_exception,
                read: codingbox_obj.builtinRead,
                yieldLimit: 20,
                __future__: Sk.python3,
            });
            codingbox_obj.stop_execution = false;
            (Sk.TurtleGraphics || (Sk.TurtleGraphics = {})).target = codingbox_obj.canvas_id;

            if (codingbox_obj.settings["options"].includes("processing")) {
                src = codingbox_obj.prepare_processing(src)
                Sk.p5 = {
                    node: codingbox_obj.canvas_id
                }
                window.onerror = function(message, source, lineno, colno, error) {
                    codingbox_obj.write_to_console("runtime error")
                }
                window.onunhandledrejection = function(e) {
                    codingbox_obj.write_to_console(e.reason)
                }
            } else {
                Sk.canvas = codingbox_obj.canvas_id;
            }
            try {
                codingbox_obj.running = true;
                var myPromise = Sk.misceval.asyncToPromise(() =>
                    Sk.importMainWithBody("<stdin>", false, src, true), {
                        "*": () => {
                            if (codingbox_obj.stop_execution) throw "Execution interrupted"
                        }
                    }
                ).catch(err => {
                    var textarea = document.querySelector("#" + codingbox_obj.console_id)
                    if (err.message) {
                        codingbox_obj.write_to_console(err.message);
                        codingbox_obj.write_to_console(err.stack);
                    } else if (err.nativeError) {
                        codingbox_obj.write_to_console(err.nativeError.message);
                        codingbox_obj.write_to_console(err.nativeError.stack);
                    } else {
                        codingbox_obj.write_to_console(err.toString());
                        codingbox_obj.write_to_console(err.stack || "")
                    }
                }).finally(() => {

                });
            } catch (e) {
                console.log(e)
            }
        });
    }
} // end of Codingbox

function generate_settings(json, setting_list) {
    json["options"] == sanitizeHTML(setting_list) + " " + sanitizeHTML(json["options"])
    return json;
}

function transform_textareafield_to_codingbox(parent_field, index) {
    parent_field.classList.add("skulpt_runner_prepared");
    textarea = parent_field.querySelector("textarea");
    json_text = textarea.cloneNode(true).children().remove().end().text();
    obj = JSON.parse(json_text);
    set_up_textarea_fields
    code = textarea.val();
    settings = generate_settings(JSON.parse(json_firstline), parent_field.className)
    var codingbox = document.createElement('div');
    codingbox.classList.add("codingbox");
    parent_field.insertBefore(codingbox, parent_div.nextSibling); //insert after!
    set_up_codingbox_with_template(codingbox, code, index);
}

// Main Setup Code
function setup_all_codingboxes(path) {
    var codingboxes = [];
    document.querySelectorAll('code.language-skulpt_runner').forEach(function(codefield, i) {
        codingbox = new CodingBox("codefield", codefield, i, path)
        codingboxes.push(codingbox)
    });
    document.querySelectorAll('code.skulpt_runner').forEach(function(codefield, i) {
        codingbox = new CodingBox("codefield", codefield, i, path)
        codingboxes.push(codingbox)
    });
    document.querySelectorAll('div.skulpt_textarea').forEach(function(textarea, i) {
        codingbox = new CodingBox("textarea", textarea, i, path);
        codingboxes.push(codingbox)
    });
    return codingboxes;
}

function init(path) {
    requirejs.config({
        baseUrl: path,
        enforceDefine: false,
        shim: {
            "bootstrap": {
                deps: ["jquery"],
                exports: "Bootstrap"
            }
        },
        paths: {
            "ace": "lib/ace",
            "jquery": "https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min",
            "p5": "lib/p5/p5.min",
            "bootstrap": "https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min",
            "skulpt": "lib/skulpt/skulpt.min",
            "skulpt-stdlib": "lib/skulpt/skulpt-stdlib",
            "filesaver": "lib/filesaver/FileSaver.min",
        }

    })
    requirejs(["p5", "jquery", "bootstrap", "filesaver"], function(p5, $, b, filesaver) {
        skulpt_runner(path);
    });

}

function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

function skulpt_runner(path) {
    var codingboxes = setup_all_codingboxes(path);
    codingboxes.forEach(function(codingbox) {
        codingbox.process();
    });
    if (document.querySelector("body").classList.contains("skulpt_runner")) {}
    // load css
    [(path || "") + 'lib/bootstrap/bootstrap_custom.css', (path || "") + 'src/skulpt_runner.css'].forEach(function(css_file) {
        loadCss(css_file)
    });
}

define("skulpt_runner", function() {
    var skulpt_runner = {}
    skulpt_runner.version = 1
    return init

});