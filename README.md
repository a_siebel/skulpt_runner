# Skulpt Runner

Brython Coderunner transforms your Markdown fields and textareas into an executable IDE. 
You can execute code immediately and even use Turtle or Processing inside the IDE.

## Demo (german):

[My Homepage](http://projects.opencoding.de/skulpt_runner/) | See Test Folder


## Usage

With the following code you can integrate skulpt_runner into your website:

<script>
    requirejs.config({
        paths: {
            "marked": "https://cdn.jsdelivr.net/npm/marked/lib/marked",
            "skulpt_runner": "../src/skulpt_runner",
        }
    })

    require(["marked"], function(marked) {
        document.querySelector("body").innerHTML = marked(document.querySelector(".marked").innerHTML);
        var path = "../"
        requirejs.config({
            paths: {
                "skulpt_runner": path + "src/skulpt_runner",
            }
        })
        requirejs(["skulpt_runner"], function(sr) {
            console.info("before init", sr);
            sr(path);
        });
    });
</script>

The code converts all tags with the .marked class to Markdown and automatically creates a skulpt_runner IDE for code fields with the following form:

````
```skulpt_runner
[python_code]
```
````

## Skulpt_runner without markdown

Code fields with the following classes are transformed in IDES:

```
<code class="language-skulpt_runner>
</code>
```

or 

```
<code class="skulpt_runner">
</code>
```

Textareas can also be converted. To do this, the textareas must be enclosed by a div in the following form:

```
<div class="skulpt_textarea">
<textarea>
</textarea>
</div>
```

### Settings

Settings can be passed to customize the appearance and functionality. The most elegant way to do this is to pass a JSON field right after the opening tag:

```
<code class="skulpt_runner">
{options="processing buttons editor"}
</code>
```

#### Variables

* **options**:
  
  * **editor**: Displays the editor.
  
  * **buttons** Displays buttons (Run, Clear).
  
  * **canvas** Displays a canvas where code can be rendered.
  
  * **processing**: Loads the code as processing code.
  
  * **turtle** / Transforms import turtle in brython (loads libraries and turtle-canvas) (alpha)

* **display**:
  
  * **sketch**: Hides code.
  
  * **fullscreen**: Shows IDE in fullscreen

* **preset**:
  
  * **processing**: Set options to editor buttons canvas processing
  
  * **turtle**: Set options to editor buttons canvas processing

### Moodle integration

You can use Brython Coderunner as moodle database activity (future) or embed it with a moodle-text-block. See moodle directory. (Currently the block must be embed in admin->additionalhtml->Before end of Body)

